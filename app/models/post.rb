class Post < ActiveRecord::Base
  has_many :comments, dependent: :destroy                                                                       # comments belong to post class
  validates_presence_of :title
  validates_presence_of :body
end
